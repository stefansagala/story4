"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('about_me/', views.about_me, name='about_me'),
    path('gallery/', views.gallery, name='gallery'),
    
    path('schedule/', views.schedule, name='schedule'),
    path('schedule_create/', views.schedule_create, name='schedule_create'),
    path('schedule_delete/', views.schedule_delete, name='schedule_delete')

]
