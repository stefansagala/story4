from django.db import models
from datetime import datetime, timedelta

# Create your models here.


class Schedule(models.Model):

    kegiatan = models.CharField(max_length=30)
    hari = models.DateTimeField(default=datetime.now)
    waktu = models.TimeField()
    tempat = models.CharField(max_length=20)
    kategori = models.CharField(max_length=20)

    # def __str__(self):
    #     return self.activity
