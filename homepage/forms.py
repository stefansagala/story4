from django import forms
from .models import Schedule


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = [
         'kegiatan',
         'hari', 
         'waktu', 
         'tempat', 
         'kategori'
         ]
        widgets = {
            'hari': forms.DateInput(attrs={'type': 'date'}),
            'waktu': forms.TimeInput(attrs={'type': 'time'})}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
