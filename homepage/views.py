from django.shortcuts import render, redirect
from .models import Schedule
# from .forms import ScheduleForm
from . import forms

# Create your views here.
def story3 (request):
    return render(request, 'story3.html')

def about_me (request):
    return render(request, 'about_me.html')

def gallery (request):
    return render(request, 'gallery.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('hari')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
   
	return redirect('homepage:schedule')
